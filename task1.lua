[[ ��� �������� ������ ������������ �����, � ������� ����������� ��������� ����� �����.
��������� ������� �� ������� ���������� ���������� ����� (�������), ���� ����� ���������� ������ ��� ����� ���� ]]

LENGTH_ARRAY = 10
MAX_VALUE = 10

arr = {}

function fill_array(arr)
	for n=1, LENGTH_ARRAY do
		--math.randomseed()
		arr[n] = math.random(1, MAX_VALUE)
		n = n + 1
	end
end

function print_array(arr)
	for k=1, #arr do
		if k ~= #arr then
			io.write(arr[k]..', ')
		else
			io.write(arr[k])
		end
		k = k + 1
	end
	io.write('\n')
end

function find_sequence(start1, start2)
	if (start1<#arr) or (start2<#arr) then
		for x=start1, #arr-1 do
			for y=start2, #arr do
				if arr[x] == arr[y] then
					seq_count = seq_count + 1
					find_sequence(start1+1, start2+1)
				end
			end
		end
	end
end


fill_array(arr)
io.write('start: ')
print_array(arr)

for i=1, #arr-1 do
	for j=i+3, #arr do
		if arr[i] == arr[j] then
			seq_count = 1
			find_sequence(i, j)
			if seq_count > 2 then
				for m=j, j+seq_count do
					arr[m] = nil
				end
			end
		end
	end
end
io.write('end: ')
print_array(arr)
