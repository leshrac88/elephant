[[ ��� �������� ������ ������������ �����, � ������� ����������� ������������ ������� ��� �����.
��������� �� ���� ������ �������� �� ���� ����� ������, ��� ��� ��������������� ���������� ��������� ������� (������ shuffle) ]]

LENGTH_ARRAY = 20
MAX_VALUE = 10

arr = {}

function fill_array(arr)
	for n=1, LENGTH_ARRAY do
		arr[n] = math.random(1, MAX_VALUE)
		n = n + 1
	end
end

function print_array(arr)
	for k=1, #arr do
		if k ~= #arr then
			io.write(arr[k]..', ')
		else
			io.write(arr[k])
		end
		k = k + 1
	end
	io.write('\n')
end

function shuffle(arr)
	length_array = #arr
	while length_array > 1 do
		i = math.random(1, length_array)
		arr[i], arr[length_array] = arr[length_array], arr[i]
		length_array = length_array - 1
	end
end

fill_array(arr)
print_array(arr)
shuffle(arr)
print_array(arr)
