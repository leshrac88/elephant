[[ � ������������� ������� ��������� ���� ����� (�������), ������ ������� ����� ���������� x0 � y0, � ����� x1 � y1.
��������� ��������� �� ����� x0,y0 ���������������� ����� ����������� ����� (������ ������� �������).
�� ����� x1,y1 ��������� ����� ��� ����� 45 �������� (�� ������� �������) � �������� ����� �������� �����.
��������� - ������� �������� ��������� ������ ����������� ����� (x2,y2 � x3,y3) ]]

require("iuplua")
require("iupluacontrols")
require("iuplua_pplot")

require("cdlua")
require("iupluacd")

require("string")

--�������� ��� ���������� �������
global_legend = {}

--�������� ��� ������ ����, ����� ������� �������
checkboxes = {}

--������� ��������� ������� ��� �������������� �������� ����
global_legend[1] = iup.text{}
global_legend[2] = iup.text{}
global_legend[3] = iup.text{}
global_legend[4] = iup.text{}

--������, ���������� �� ������ ���������� �������
plot_btn = iup.button{ title = "Draw" }

function transform1(start, finish, angle, pwidget)
	c1 = start[1]*(1-math.cos(angle)) + start[2]*math.sin(angle)
	c2 = start[2]*(1-math.cos(angle)) - start[1]*math.sin(angle)

	x2 = finish[1]*math.cos(angle) - finish[2]*math.sin(angle) + c1
	y2 = finish[1]*math.sin(angle) + finish[2]*math.cos(angle) + c2

	draw_plot(pwidget, { start[1], x2 }, { start[2], y2 })
end

function transform2(start, finish, angle, pwidget)
	c1 = start[1]*(1-math.cos(angle)) + start[2]*math.sin(angle)
	c2 = start[2]*(1-math.cos(angle)) - start[1]*math.sin(angle)

	x2 = (finish[1]*math.cos(angle) - finish[2]*math.sin(angle) + c1)/2
	y2 = (finish[1]*math.sin(angle) + finish[2]*math.cos(angle) + c2)/2

	draw_plot(pwidget, { x1, x2 }, { y1, y2 })
end

--������� ������ �� �������� ������ �� ��������� ������
function draw_plot(pwidget, x, y)

	iup.PPlotBegin(pwidget, 0)
	for i=1, #x do
		iup.PPlotAdd(pwidget, x[i], y[i])
	end

	iup.PPlotEnd(pwidget)
end


--������� ��� ������ ���������� �������
function plot_btn:action()

	--������� ������ ��� �������
	plot = iup.pplot
	{
		expand = "YES",
		TITLE = "Simple line",
		MARGINBOTTOM = "65",
		MARGINLEFT = "65",
		LEGENDSHOW = "YES",
		LEGENDPOS = "TOPLEFT",
		size = "400x400"
	}

	iup.PPlotBegin(plot, 0)
	iup.PPlotAdd(plot, 0, 0)
	plot.DS_LEGEND = ""
	iup.PPlotEnd(plot)

	x0 = global_legend[1].value
	y0 = global_legend[2].value
	x1 = global_legend[3].value
	y1 = global_legend[4].value
	draw_plot(plot, { x0, x1 }, { y0, y1 })
	if checkboxes[1].value == "ON" then
		transform1({ x0, y0 }, { x1, y1 }, -math.pi/2, plot)
	end
	if checkboxes[2].value == "ON" then
		transform2({ x1, y1 }, { x0, y0 }, math.pi/4, plot)
	end

	--������� ���� ��� ����������� �������
	plot_dg = iup.dialog
	{ iup.vbox{ plot } }

	--���������� ������ � ��������
	plot_dg:showxy(iup.CENTER, iup.CENTER)

	--��������� ����� ��������� ������� ��� �������
	if (iup.MainLoopLevel() == 0) then
		iup.MainLoop()
	end
end


--������� �������, ������� ����� ��������� �� ��, ����� �� ������� ������
checkboxes[1] = iup.toggle{ title = "������ 1", value = "ON" }
checkboxes[2] = iup.toggle{ title = "������ 2", value = "ON" }

--������� ����� ��� ����� �������� �������
frame = iup.frame
{
	iup.vbox
	{
		iup.label
		{
			title = "������������ ������:",
			expand = "HORIZONTAL"
		},
		iup.vbox{ unpack(checkboxes) },
		iup.label{},
		iup.hbox{ iup.label{ title = "X0: " }, global_legend[1] },
		iup.hbox{ iup.label{ title = "Y0: " }, global_legend[2] },
		iup.hbox{ iup.label{ title = "X1: " }, global_legend[3] },
		iup.hbox{ iup.label{ title = "Y1: " }, global_legend[4] },
		iup.label{},
		plot_btn
	};
	expand = "VERTICAL"
}

--������� ������� ���� ��������� � ������� �� ���� ���������
dg = iup.dialog
{
	iup.hbox{ frame },
	title = "������ ������",
	size = "HALF"
}

--���������� ������� ���� � ��������� ��������� �������
dg:showxy(iup.CENTER, iup.CENTER)
if (iup.MainLoopLevel() == 0) then
	iup.MainLoop()
end
